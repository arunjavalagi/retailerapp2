(function () {
    "use strict";

    angular.module("myapp.controllers", ['ionic', 'ngCordova'])

    .controller("appCtrl", ["$scope", "$http", "$customlocalstorage", function ($scope, $http, $customlocalstorage) {

    }])

    .controller("homeCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$ionicLoading", "$http", "$ionicPopover", "$ionicPopup", "$filter", "$ionicHistory", "$popupService", "$rootScope", "$cordovaToast", function ($scope, $state, $customlocalstorage, $config, $ionicLoading, $http, $ionicPopover, $ionicPopup, $filter, $ionicHistory, $popupService, $rootScope, $cordovaToast) {
        $scope.refresh = function () {
            $scope.$broadcast("scroll.refreshComplete");
        };
        $scope.canMapProducts = $customlocalstorage.getObject('retailer').canMapProducts;
        if (localStorage['tokenUpdated'] === undefined && !angular.isUndefined(localStorage['token'])) {
            console.log(localStorage['token']);
            $http.put('http://' + $config.IP_PORT + '/retailer/updateToken',
                {
                    "token": localStorage['token'],
                    "id": localStorage['retailerID']
                }).then(function (res) {
                    console.log(res);

                    if (res.data.status === "1") {
                        console.log('token updated');
                        localStorage['tokenUpdated'] = 'done';
                        //  $popupService.showAlert('Token Update Success');
                    }
                    else {
                    }
                }, function (err) {
                    console.log(err);
                    $popupService.showAlert('Token Update failed', JSON.stringify(err));
                    console.log('token update failed');
                });
        }
        else {
            console.log('token already updated');
            //$popupService.showAlert('Token aleardy updated', localStorage['token']);
        }
        $scope.allStatus = [];
        $scope.data = {
            filterChoice: 'No Filter',
            orderChoice: 'Default Order'
        }
        $scope.orderList = [];
        $scope.loadOrderList = function () {
            $ionicLoading.show({
                template: 'Loading orders...',
                duration: 3
            });
            $http.get('http://' + $config.IP_PORT + '/order/retailer/' + localStorage['retailerID']).then(function (res, status) {
                console.log(res);
                $scope.orderList = res.data.slice(0, res.data.length - 1);
                $scope.allStatus = res.data[res.data.length - 1];
                $rootScope.newOrderCount = ($filter('filter')($scope.orderList, { status: 'NEW' })).length;
                if ($scope.data.filterChoice !== "No Filter") {
                    $scope.orderList = $filter('filter')($scope.orderList, { status: $scope.data.filterChoice.toUpperCase() });
                }

                if ($scope.data.orderChoice == 'Date Descending') {
                    $scope.orderList = $filter('orderBy')($scope.orderList, ['orderDateInMilli'], 'reverse');
                }
                else if ($scope.data.orderChoice == 'Date Ascending') {
                    $scope.orderList = $filter('orderBy')($scope.orderList, ['orderDateInMilli']);
                }
                $ionicLoading.hide();
            },
            function (err) {
                console.log(err);
                if (err.data.code === "ENOTFOUND") {
                    $cordovaToast.showShortBottom('Internet Error', 'Oops! Internet is not working');
                }
                $ionicLoading.hide();
            })
        };
        $scope.loadOrderList();
        $scope.consumerDetail = '';
        $scope.onConsumerClick = function (order) {
            $ionicLoading.show({
                template: 'Loading Consumer Detail..'
            });
            $http.get('http://' + $config.IP_PORT + '/consumer/id/' + order.consumerId).then(function (res, status) {
                $ionicLoading.hide();
                $scope.consumerDetail = res.data;
                $ionicPopup.alert({
                    template: '<img  class="full-image" style="width:100%;" fallback-src="user-male.png" src="http://' + $config.IP_PORT + '/consumer/image/' + $scope.consumerDetail.id + '"/> <p><b>Phone:</b> ' + $scope.consumerDetail.phoneNumber + '</p><p><b>Email:</b> ' + $scope.consumerDetail.mail_id + '</p><p><b>Address:</b> ' + $scope.consumerDetail.address + '</p>',
                    title: '<h3>' + $scope.consumerDetail.name + '</h3>',
                });
            }, function (err) {
                $cordovaToast.showLongBottom('Error getting consumer details');
            })
        };
        $scope.onOrderClick = function (ord) {
            $state.go('orderDetail', { order: ord, allStatus: $scope.allStatus });
        };
        $scope.filterBy = function () {
            var filterPopup = $ionicPopup.show({
                templateUrl: 'filter-popup.html',
                title: 'Filter by',
                scope: $scope
            });
            $scope.clicked1 = function () {
                console.log($scope.data.filterChoice);
                filterPopup.close();
                $scope.loadOrderList();
            };
        };
        $scope.filterByNew = function () {

        }
        $scope.orderBy = function () {
            var orderByPopup = $ionicPopup.show({
                template: '<ion-list><ion-radio value="Default Order" ng-model="data.orderChoice" ng-click="clicked2()">Default Order</ion-radio><ion-radio value="Date Descending" ng-model="data.orderChoice" ng-click="clicked2()">Date Descending</ion-radio><ion-radio value="Date Ascending" ng-model="data.orderChoice" ng-click="clicked2()">Date Ascending</ion-radio></ion-list>',
                title: 'Order by',
                scope: $scope
            });
            $scope.clicked2 = function () {
                console.log($scope.data.orderChoice);
                orderByPopup.close();
                $scope.loadOrderList();
            };
        };
        $ionicPopover.fromTemplateUrl('my-popover.html', {
            scope: $scope
        })
        .then(function (popover) {
            $scope.popover = popover;
        });
        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
            console.log("openPo-pover");
        };
        $scope.closePopover = function () {
            $scope.popover.hide();
            console.log("closePopover");
        };
        $scope.$on('$destroy', function () {
            $scope.popover.remove();
            console.log("$destroy");
        });
        $scope.$on('popover.hidden', function () {
            console.log("hidden");
        });
        $scope.$on('popover.removed', function () {
            console.log("removed");
        });
    }])

    .controller("newordersCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$ionicLoading", "$http", "$ionicPopover", "$stateParams", "$cordovaPrinter", function ($scope, $state, $customlocalstorage, $config, $ionicLoading, $http, $ionicPopover, $stateParams, $cordovaPrinter) {

    }])

    .controller("orderDetailCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$ionicLoading", "$http", "$ionicPopover", "$stateParams", "$popupService", "$rootScope", "$filter", '$cordovaToast', '$ionicPopup', function ($scope, $state, $customlocalstorage, $config, $ionicLoading, $http, $ionicPopover, $stateParams, $popupService, $rootScope, $filter, $cordovaToast, $ionicPopup) {
        $scope.orderDetail = {};
        $scope.status = {
            availableStatus: []
        }
        $scope.rawOrderDetail = [];
        $scope.order = $stateParams.order;
        $scope.allStatus = $stateParams.allStatus;
        $scope.dataToPrint = {};
        var dataToShow = {};
        $scope.consumerDetail = {};
        $scope.loadOrderDetail = function () {
            $ionicLoading.show({
                template: 'Loading order detail...'
            });
            $http.get('http://' + $config.IP_PORT + '/order/order_id/' + $scope.order.orderId).then(function (res, status) {
                $scope.rawOrderDetail = res.data;
                $ionicLoading.hide();
                $scope.status.availableStatus = $scope.allStatus.slice($scope.allStatus.indexOf($scope.rawOrderDetail.status));
                $scope.status.selected = $scope.rawOrderDetail.status;
            },
            function (err) {
                console.log(err);
                $ionicLoading.hide();
            });
        };
        $scope.statusChange = function (selection) {
            if (selection == 'RECEIVED') {
                $cordovaToast.showLongBottom('RECEIVED status can only be set by Consumer. You can select DELIVERED instead');
                $scope.status.selected = $scope.status.availableStatus[0];
                return;
            }
            $ionicLoading.show({ template: 'Updating status..' });
            $http.put('http://' + $config.IP_PORT + '/order/changeStatus/' + $scope.order.orderId + '/' + selection).then(function (res) {
                if (res.data[0].status === 'OK') {
                    $ionicLoading.hide();
                    $cordovaToast.showShortBottom('Order status Changed');
                    $scope.status.availableStatus = $scope.allStatus.slice($scope.allStatus.indexOf(selection));
                    $scope.status.selected = selection;
                }
                else {
                    $cordovaToast.showLongBottom('Unable to change the order status');
                    $scope.status.selected = $scope.status.availableStatus[0];
                }
            }, function (err) {
                $cordovaToast.showLongBottom('Error uploading status');
                $scope.status.selected = $scope.status.availableStatus[0];
            });
        };
        $scope.loadOrderDetail();
        $scope.onConsumerClick = function (order) {
            $ionicLoading.show({
                template: 'Loading Consumer Detail..'
            });
            $http.get('http://' + $config.IP_PORT + '/consumer/id/' + order.consumerId).then(function (res, status) {
                $ionicLoading.hide();
                $scope.consumerDetail = res.data;
                $ionicPopup.alert({
                    template: '<img  class="full-image" style="width:100%;" fallback-src="user-male.png" src="http://' + $config.IP_PORT + '/consumer/image/' + $scope.consumerDetail.id + '"/> <p><b>Phone:</b> ' + $scope.consumerDetail.phoneNumber + '</p><p><b>Email:</b> ' + $scope.consumerDetail.mail_id + '</p><p><b>Address:</b> ' + $scope.consumerDetail.address + '</p>',
                    title: '<h3>' + $scope.consumerDetail.name + '</h3>',
                });
            }, function (err) {
                $cordovaToast.showLongBottom('Error getting consumer details');
            })
        };
        var orderStatusCount = 0;
        $scope.printPreviewData = function (res) {
            $state.go('printPreview', { orderDetail: $scope.rawOrderDetail });
        }
    }])

    .controller("printCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$ionicLoading", "$http", "$ionicPopover", "$stateParams", "$cordovaPrinter", '$cordovaToast', function ($scope, $state, $customlocalstorage, $config, $ionicLoading, $http, $ionicPopover, $stateParams, $cordovaPrinter, $cordovaToast) {
        $scope.orderDetail = $stateParams.orderDetail;
        $scope.printPage = function () {

            if (!$cordovaPrinter.isAvailable()) {
                $cordovaToast.showLongBottom('Printing is not available on device');
            } else {
                var content = $('#printArea').html();
                $cordovaPrinter.print(content);
            }
        }
    }])

    .controller("loginCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$http", "$ionicLoading", "$popupService", "$ionicHistory", function ($scope, $state, $customlocalstorage, $config, $http, $ionicLoading, $popupService, $ionicHistory) {
        $scope.refresh = function () {
            $scope.$broadcast("scroll.refreshComplete");
        };

        $scope.loginDetail = [];
        $scope.data = {};
        $scope.loginData = {
            password: '',
            mobileNo: ''

        }
        $scope.login = function () {
            $scope.loginDetail = [];
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $ionicLoading.show({
                template: 'Logging in...Please wait...'
            });
            $http.post('http://' + $config.IP_PORT + '/retailer/login', $scope.loginData).then(function (res) {
                console.log(res);
                $scope.loginDetail = res.data;
                if (res.data.status === '1') {
                    $popupService.showAlert('Login Success', res.data.message).then(function () {
                        localStorage['retailerID'] = res.data.retailerId;
                        $http.get('http://' + $config.IP_PORT + '/retailer/' + $customlocalstorage.get('retailerID')).then(function (res) {
                            if (res.data.isApproved === 0) {
                                console.log('not approved');
                                $ionicLoading.show({
                                    template: 'Your Store is not approved by NABL!Contact Administrator!'
                                });

                            }
                            else {
                                console.log('Approved');
                                $customlocalstorage.setObject('retailer', res.data);
                                localStorage['loggedIn'] = true;
                                $scope.loginDetail = [];
                                $ionicHistory.nextViewOptions({
                                    disableBack: true
                                });
                                $state.go('home');
                            }
                        });
                    });
                    var localDetail = $customlocalstorage.getObject('retailer');
                    console.log(localDetail);
                    $ionicLoading.hide();
                }
                else if (res.data.status === '0') {
                    $popupService.showAlert('Login Failed', res.data.message + " <b>Please Register</b>");
                    $ionicLoading.hide();
                    $state.go('register');
                    $ionicHistory.nextViewOptions({
                        disableBack: false

                    });
                }
                else
                {
                    $popupService.showAlert('Login Failed', res.data.message);
                    $ionicLoading.hide();
                }
            }, function (err) {
                console.log(err);
                $popupService.showAlert('Login Failed', err.data.code);
                $state.go('login');
                $ionicLoading.hide();
            });
        }
        $scope.forgotPassword = function () {
            $popupService.showConfirm("Forgot password", "The new password will be sent to you phone via SMS.", $scope.data)
            .then(function () {
                if ($scope.data.confirm) {
                    if (!$scope.loginData.mobileNo || $scope.loginData.mobileNo.toString().length != 10) {
                        $cordovaToast.showShortBottom('Please enter the valid phone number to register.');
                        return false;
                    }
                    else {
                        $http.post('http://' + $config.IP_PORT + '/user/generatePassword/retailer', { mobileNo: $scope.loginData.mobileNo }).then(function (res) {
                            $cordovaToast.showShortBottom(res.data.message);
                        });
                    }
                }
            });
        };

    }])

    .controller("registerCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$http", "$ionicLoading", "$popupService", "$cordovaGeolocation", "$cordovaToast", "$ionicHistory", function ($scope, $state, $customlocalstorage, $config, $http, $ionicLoading, $popupService, $cordovaGeolocation, $cordovaToast, $ionicHistory) {
        console.log("registerCtrl");
        $scope.form = {
        };
        $scope.register = function () {
            var reqObj = {};
            var register = function () {
                var req = {
                    method: "POST",
                    url: 'http://' + $config.IP_PORT + '/retailer/add',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify(reqObj)


                }
                $ionicLoading.show({
                    template: 'Please wait...'
                });
                $http(req).then(function (res) {
                    $ionicLoading.hide();
                    if (res.data.status == '1') {
                        $popupService.showAlert('Success', res.data.message);
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go("login");
                    }
                    else if (res.data.status === '-1') {
                        $popupService.showAlert('Registeration Fail', res.data.message);
                        $state.go('register')
                    }
                    else {
                        $popupService.showAlert(' Registeration Fail', res.data.message);
                        $state.go('register')
                    }

                }, function (err) {
                    console.warn(err);
                    $ionicLoading.hide();
                    $state.go('login');
                });
            };
            var reqObj = {
                retailer: {
                    storename: $scope.form.storename,
                    name: $scope.form.name,
                    mailId: $scope.form.email,
                    latitude: 11.11,
                    longitude: 44.14,
                    imageName: "123.jpg",
                    tin: $scope.form.TIN,
                    storeaddress: {
                        zipCode: $scope.form.zipcode,
                        street: $scope.form.street,
                        city: {
                            id: 1
                        },
                        state: {
                            id: 17
                        }
                    }
                },
                retailerPhoneList: [{
                    type: "MOBILE",
                    contactType: "PRIMARY",
                    phoneNumber: $scope.form.mobileNo
                }]
            }
            var options = {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
            };
            navigator.geolocation.getCurrentPosition(function (position) {
                $cordovaToast.showShortBottom('Lat:' + position.coords.latitude + ' long' + position.coords.longitude);
                reqObj.retailer.latitude = position.coords.latitude;
                reqObj.retailer.longitude = position.coords.longitude;
                register();
            }, function () {
                $cordovaToast.showShortBottom('Error getting geo location.');
            }, options);
        };
    }])

    .controller("invitationCtrl", ["$scope", "$state", "$customlocalstorage", "$config", "$http", "$ionicLoading", "$popupService", function ($scope, $state, $customlocalstorage, $config, $http, $ionicLoading, $popupService) {
        $scope.refresh = function () {
            $scope.$broadcast("scroll.refreshComplete");
        };

        $scope.data = {
            phoneno: ''
        }

        $scope.sendInvitation = function () {
            if ($scope.data.phoneno == null || $scope.data.phoneno.toString().length != 10) {
                $popupService.showAlert('Invalid!', 'Please enter 10 digit mobile number.');
                return;
            }

            $ionicLoading.show({
                template: 'Inviting customer...'
            });
            $http.post('http://' + $config.IP_PORT + '/invitation/add', { mobileNo: $scope.data.phoneno, id: localStorage['retailerID'] }).success(function (res) {
                if (res.status === '1') {
                    console.log('invitation success');
                    $popupService.showAlert('Success', res.message);
                    $scope.data.phoneno = null;
                }
                else {
                    console.log('invitation failed');
                    $popupService.showAlert('Failed!', res.message);
                    $scope.data.phoneno = null;
                }
                console.log(res);
                $ionicLoading.hide();
                $scope.data.phoneno = null;
            }).then(function (err) {
                console.log(err);
                $ionicLoading.hide();
            })
        };
    }])

    .controller("settingsCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "$popupService", "$ionicHistory", function ($scope, $state, $customlocalstorage, $http, $popupService, $ionicHistory) {
        console.log('settingsCtrl');
        $scope.localStoragePair = [];

        angular.forEach(localStorage, function (v, i) {
            $scope.localStoragePair.push({ key: i, value: v });
        });

        $scope.resetData = function () {
            $scope.data = {
                confirm: false
            }

            $popupService.showConfirm("Reset Confirm", "Are you sure, You want to reset the Smart Retailer data!", $scope.data).then(function () {
                if ($scope.data.confirm) {
                    localStorage.removeItem('loggedIn');
                    localStorage.removeItem('retailer');
                    localStorage.removeItem('retailerID');
                    localStorage.removeItem('tokenUpdated');
                    $popupService.showAlert("Reset Done!", "All the app related data has beed removed. Including logged in details.").then(function () {
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go('login');
                    });


                }
            });
        }
    }])

    .controller("addproductsCtrl", ["$scope", "$state", "$customlocalstorage", "$http", "$popupService", "$ionicHistory", '$config', '$ionicLoading', '$cordovaToast', '$stateParams', '$filter', function ($scope, $state, $customlocalstorage, $http, $popupService, $ionicHistory, $config, $ionicLoading, $cordovaToast, $stateParams, $filter) {
        $scope.category = [];

        $scope.logicalGroup = [];
        $scope.unitOfMeasurement = [
        { name: 'KG - KiloGrams', value: 'kg' },
        { name: 'g - Grams', value: 'g' },
        { name: 'Liters', value: 'l' },
        { name: 'Ml - Milli liters', value: 'ml' },
        { name: 'CM - Centimeters', value: 'cs' },
        { name: 'MM - MilliMeters', value: 'mm' },
        { name: 'Quantity', value: 'qty' }];
        $scope.data = {
            isDaily: true,
            isMainProduct: true,
            isActive: true
        };
        $scope.AddProduct = function () {
            var productData = {
                name: $scope.data.productName,
                categoryId: $scope.data.category.id,
                segmentId: $scope.data.segment.id,
                subSegmentId: $scope.data.subsegment.id,
                isDaily: '1',
                logicalGroupId: $scope.data.logicalGroup.id,
                isActive: $scope.data.isActive ? 'ON' : 'OFF',
                unitOfMeasurement: $scope.data.unitOfMeasurement.value,
                brand: $scope.data.brand,
                primaryUse: $scope.data.primaryUse,
                specification: $scope.data.specification,
                rank: $scope.data.rank,
                consumerPrice: $scope.data.consumerPrice,
                dealerPrice: $scope.data.retailerPrice,
                imagesCount: 0
            }
            $ionicLoading.show({
                template: 'Adding product...'
            });
            $http.post('http://' + $config.IP_PORT + '/product/addProduct', productData).then(function (res) {
                if (res.data.status == '1') {
                    $cordovaToast.showShortBottom('New product added successfully');

                    productData.id = res.data.productId;
                    productData.category = $scope.data.category;
                    productData.segment = $scope.data.segment;
                    productData.subSegment = $scope.data.subsegment;
                    productData.logicalGroup = $scope.data.logicalGroup;
                    productData.unitOfMeasurement = $scope.data.unitOfMeasurement;

                    var addedProducts = $customlocalstorage.getObjectorDefault('addedProducts', '[]');
                    addedProducts.push(productData);
                    $customlocalstorage.setObject('addedProducts', addedProducts);

                    $http.get('http://' + $config.IP_PORT + '/product/' + res.data.productId).then(function (res) {
                        $ionicLoading.hide();
                        $state.go('addProductImage', { product: productData });
                    });
                }
                else {
                    $cordovaToast.showShortBottom('Problem adding product');
                }
                $ionicLoading.hide();
            }, function err() {
                $cordovaToast.showShortBottom('Problem adding product');
                $ionicLoading.hide();
            });
        }
        if ($stateParams.product) {
            var prod = $stateParams.product;
            $scope.data.productName = prod.name;
            //$scope.data.category = prod.category.name;
            //$scope.data.segment = prod.segment.name;
            //$scope.data.subsegment = prod.subSegment.name;
            $scope.data.logicalGroup = prod.logicalGroup;
            $scope.data.unitOfMeasurement = $scope.unitOfMeasurement.filter(function (e) { return e.value == prod.unitOfMeasurement.value })[0]; prod.unitOfMeasurement;
            $scope.data.brand = prod.brand;
            $scope.data.primaryUse = prod.primaryUse;
            $scope.data.specification = prod.specification;
            $scope.data.rank = prod.rank;
            $scope.data.consumerPrice = prod.consumerPrice;
            $scope.data.retailerPrice = prod.dealerPrice;
        }
        $http.get('http://' + $config.IP_PORT + '/category').then(function (res) {
            $scope.category = res.data;
            $scope.data.category = res.data.filter(function (e) { return e.id == prod.category.id })[0];
            $scope.data.segment = $scope.data.category.segments.filter(function (e) { return e.id == prod.segment.id })[0];
            $scope.data.subsegment = $scope.data.segment.subsegments.filter(function (e) { return e.id == prod.subSegment.id })[0];
        });
        $http.get('http://' + $config.IP_PORT + '/product/logicalgroups').then(function (res) {
            $scope.logicalGroup = res.data;
            $scope.data.logicalGroup = $scope.logicalGroup.filter(function (e) { return e.id == prod.logicalGroup.id })[0];
        });
        $scope.categoryInit = function () {
            console.log('category init');
        }
    }])

    .controller("retailerProfileCtrl", ["$scope", "$http", "$customlocalstorage", "$ionicLoading", "$ionicHistory", "$config", function ($scope, $http, $customlocalstorage, $ionicLoading, $ionicHistory, $config) {
        var retailerID = $customlocalstorage.get('retailerID');
        // console.log($scope.retailer);
        $ionicLoading.show({
            template: 'Please wait...'
        });
        $http.get('http://' + $config.IP_PORT + '/retailer/' + retailerID).then(function (rest) {
            $customlocalstorage.setObject('retailer', rest.data);
            $ionicLoading.hide();

            $scope.retailer = rest.data;
            $scope.retailer.consumerImageUrl = 'http://' + $config.IP_PORT + '/retailer/image/' + localStorage['retailerID'] + '?' + new Date().getTime();
            $scope.retailer.logoImageUrl = 'http://' + $config.IP_PORT + '/retailer/logo/' + localStorage['retailerID'] + '?' + new Date().getTime();

            var target = $('img#profile');
            ImgCache.isCached($scope.retailer.consumerImageUrl, function (path, success) {
                if (success) {
                    ImgCache.useCachedFile(target);
                    console.log('image got from cache');
                } else {
                    console.log('image not found in the catch');
                    ImgCache.cacheFile($scope.retailer.consumerImageUrl, function () {
                        ImgCache.useCachedFile(target);
                    });
                }
            });
            var logo = $('img#logo');
            ImgCache.isCached($scope.retailer.logoImageUrl, function (path, success) {
                if (success) {
                    ImgCache.useCachedFile(logo);
                    console.log('image got from cache');
                } else {
                    console.log('image not found in the catch');
                    ImgCache.cacheFile($scope.retailer.consumerImageUrl, function () {
                        ImgCache.useCachedFile(logo);
                    });
                }
            });
        }, function (err) {
            $ionicLoading.hide();
        });

    }])

    .controller("editprofileCtrl", ["$scope", "$customlocalstorage", "$http", "$cordovaImagePicker", "$cordovaContacts", '$cordovaCamera', '$popupService', '$config', '$ionicPopup', "$ionicLoading", "$cordovaToast", function ($scope, $customlocalstorage, $http, $cordovaImagePicker, $cordovaContacts, $cordovaCamera, $popupService, $config, $ionicPopup, $ionicLoading, $cordovaToast) {
        $scope.image = {
            progress: 0,
            currentImage: '',
            retailerImageUrl: "http://" + $config.IP_PORT + "/retailer/image/" + localStorage['retailerID'] + '?' + new Date()
        };
        $scope.logo = {
            progress: 0,
            currentImage: '',
            retailerLogoUrl: "http://" + $config.IP_PORT + "/retailer/logo/" + localStorage['retailerID'] + '?' + new Date()
        };
        $scope.retailer = '';
        var retailerID = $customlocalstorage.get('retailerID');
        $ionicLoading.show({ template: 'Please wait...' });
        $http.get('http://' + $config.IP_PORT + '/retailer/' + retailerID).then(function (res) {
            $customlocalstorage.setObject('retailer', res.data);
            $ionicLoading.hide();
            res.data.storeaddress.zipCode = Number(res.data.storeaddress.zipCode);
            res.data.retailerPhone[0].phoneNumber = Number(res.data.retailerPhone[0].phoneNumber);
            $scope.retailer = res.data;
           
        }, function (err) {
            $cordovaToast.showLongBottom('Error getting consumer details');
        });
        $scope.saveProfile = function () {
            $ionicLoading.show({ template: 'Saving profile...' });
            $http.post('http://' + $config.IP_PORT + '/retailer/update', $scope.retailer).then(function (res) {
                //if (res.data.status == "1") {
                //    $http.get('http://' + $config.IP_PORT + '/retailer/' + res.data.retailerID).then(function (res) {
                //        $customlocalstorage.setObject('retailer', res.data);
                //        $scope.retailer = res.data;
                //        //$scope.profile.date_of_birth = $filter('date')(new Date(res.data.date_of_birth), 'dd-MM-yyyy');

                //    }, function (err) {
                //        ionicToast.show('Unable to get consumer details after registration.', 'bottom', false, 3000);
                //    });

                //}
                $ionicLoading.hide();
                $cordovaToast.showLongBottom('Profile saved successfully');
            }, function (err) {
                $cordovaToast.showLongBottom('Error saving the profile.');
                $ionicLoading.hide();
            });
        }
        $scope.imageData = '';
        var selectionPopup;
        var options = {};
        $scope._updateImage = function () {
            $scope.data = {};
            var uploadImage = function () {
                $cordovaCamera.getPicture(options).then(function (imageData) {
                    console.log(imageData);
                    $scope.imageData = imageData;

                    var opt = new FileUploadOptions();
                    opt.fileKey = "image_file";
                    opt.fileName = imageData.substr(imageData.lastIndexOf('/') + 1);
                    opt.mimeType = "image/jpeg";
                    opt.params = {
                        retailer_id: localStorage['retailerID']
                    };

                    var ft = new FileTransfer();
                    ft.onprogress = function (progressEvent) {
                        $scope.image.progress = JSON.stringify(progressEvent);
                        console.log(progressEvent);
                    };

                    $ionicLoading.show({
                        template: 'Uploading profile pic...',
                        duration: 20000
                    })
                    ft.upload(imageData, encodeURI("http://" + $config.IP_PORT + "/retailer/uploadImageFile"),
                    function (res) {
                        console.log(res);
                        $ionicLoading.hide();
                        $cordovaToast.show('Profile image uploaded.', 'bottom', false, 3000);
                    },
                    function (err) {
                        console.log(err);
                        $ionicLoading.hide();
                        $cordovaToast.show('Error uploading the profile image. Please try again.', 'middle', false, 4000);
                    }, opt);

                }, function (err) {
                    console.log(err);
                    $cordovaToast.show('Error getting image from device.', 'bottom', false, 3000);
                });
            };
            $scope.getFromCamera = function () {
                selectionPopup.close();
                options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.NATIVE_URI,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 512,
                    targetHeight: 512,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true,
                    correctOrientation: true
                };
                uploadImage();
            };
            //$scope.getFromFile = function () {
            //    selectionPopup.close();
            //    options = {
            //        quality: 50,
            //        destinationType: Camera.DestinationType.FILE_URI,
            //        sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
            //        allowEdit: true
            //    };
            //    uploadImage();
            //};

            selectionPopup = $ionicPopup.show({
                template: '<div class="list card"><a class="item item-icon-left" ng-click="getFromCamera()"><i class="icon ion-camera"></i>Camera</a></div>',
                title: 'Take image from?',
                scope: $scope
            });
        };
        $scope.updateImage = function () {
            $scope.data = {};
            var uploadImage = function () {
                $cordovaCamera.getPicture(options).then(function (imageData) {
                    $scope.imageData = imageData;

                    var opt = new FileUploadOptions();
                    opt.fileKey = "image_file";
                    opt.fileName = imageData.substr(imageData.lastIndexOf('/') + 1);
                    opt.mimeType = "image/jpeg";
                    opt.params = {
                        retailer_id: $scope.retailer.id,
                    };

                    var ft = new FileTransfer();
                    ft.onprogress = function (progressEvent) {
                        $scope.image.progress = JSON.stringify(progressEvent);
                        console.log(progressEvent);
                    };

                    $ionicLoading.show({
                        template: 'Uploading profile image...',
                        duration: 20000
                    })
                    ft.upload(imageData, encodeURI("http://" + $config.IP_PORT + "/retailer/uploadImageFile"),
                    function (res) {
                        $ionicLoading.hide();
                        $cordovaToast.showShortBottom('Profile image uploaded');
                        $scope.image.retailerImageUrl = "http://" + $config.IP_PORT + "/retailer/image/" + localStorage['retailerID'] + '?' + new Date()
                    },
                    function (err) {
                        $ionicLoading.hide();
                        $cordovaToast.showShortBottom('Error uploading the profile image. Please try again later.');
                    }, opt);

                }, function (err) {
                    console.log(err);
                    $cordovaToast.show('Error getting image from device.', 'bottom', false, 3000);
                });
            };
            $scope.getFromCamera = function () {
                selectionPopup.close();
                options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.NATIVE_URI,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 400,
                    targetHeight: 400,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true,
                    correctOrientation: true
                };
                uploadImage();
            };
            $scope.getFromFile = function () {
                selectionPopup.close();
                options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                    allowEdit: true
                };
                uploadImage();
            };

            selectionPopup = $ionicPopup.show({
                template: '<div class="list card"><a class="item item-icon-left" ng-click="getFromCamera()"><i class="icon ion-camera"></i>Camera</a><a class="item item-icon-left" ng-click="getFromFile()"><i class="icon ion-image"></i>Existing file</a></div>',
                title: 'Take image from?',
                scope: $scope
            });
        };

        $scope.updateLogo = function () {
            $scope.data = {};
            var uploadLogo = function () {
                $cordovaCamera.getPicture(options).then(function (imageData) {
                    $scope.imageData = imageData;

                    var opt = new FileUploadOptions();
                    opt.fileKey = "logo_file";
                    opt.fileName = imageData.substr(imageData.lastIndexOf('/') + 1);
                    opt.mimeType = "image/jpeg";
                    opt.params = {
                        retailer_id: $scope.retailer.id,
                    };

                    var ft = new FileTransfer();
                    ft.onprogress = function (progressEvent) {
                        $scope.logo.progress = JSON.stringify(progressEvent);
                        console.log(progressEvent);
                    };

                    $ionicLoading.show({
                        template: 'Uploading logo image...',
                        duration: 20000
                    })
                    ft.upload(imageData, encodeURI("http://" + $config.IP_PORT + "/retailer/uploadLogoFile"),
                    function (res) {
                        $ionicLoading.hide();
                        $cordovaToast.showShortBottom('Logo image uploaded');
                        $scope.logo.retailerLogoUrl = "http://" + $config.IP_PORT + "/retailer/logo/" + localStorage['retailerID'] + '?' + new Date()
                    },
                    function (err) {
                        $ionicLoading.hide();
                        $cordovaToast.showShortBottom('Error uploading the logo image. Please try again later.');
                    }, opt);

                }, function (err) {
                    console.log(err);
                    $cordovaToast.show('Error getting logo image from device.', 'bottom', false, 3000);
                });
            };
            $scope.getLogoFromCamera = function () {
                selectionPopup.close();
                options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.NATIVE_URI,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 400,
                    targetHeight: 400,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true,
                    correctOrientation: true
                };
                uploadLogo();
            };
            $scope.getLogoFromFile = function () {
                selectionPopup.close();
                options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                    allowEdit: true
                };
                uploadLogo();
            };

            selectionPopup = $ionicPopup.show({
                template: '<div class="list card"><a class="item item-icon-left" ng-click="getLogoFromCamera()"><i class="icon ion-camera"></i>Camera</a><a class="item item-icon-left" ng-click="getLogoFromFile()"><i class="icon ion-image"></i>Existing Logo</a></div>',
                title: 'Take logo from?',
                scope: $scope
            });
        };


    }])

    .controller("productManagementCtrl", ['$scope', '$config', '$http', function ($scope, $config, $http) {

    }])

    .controller('addedProductsCtrl', ['$scope', '$http', '$config', '$customlocalstorage', '$ionicLoading', '$state', function ($scope, $http, $config, $customlocalstorage, $ionicLoading, $state) {
        $scope.products = $customlocalstorage.getObject('addedProducts');
        $scope.productDetail = function (item) {
            $state.go('productDetail', { product: item });
        }
    }])

    .controller('productDetailCtrl', ['$scope', '$http', '$config', '$customlocalstorage', '$ionicLoading', '$stateParams', '$state', function ($scope, $http, $config, $customlocalstorage, $ionicLoading, $stateParams, $state) {
        $scope.product = $stateParams.product;
        $scope.gotoProductImages = function () {
            $state.go('addProductImage', { product: $scope.product });
        }
        $scope.clone = function () {
            $state.go('addproducts', { product: $scope.product });
        }
    }])

    .controller('addProductImageCtrl', ['$scope', '$http', '$config', '$customlocalstorage', '$ionicLoading', '$stateParams', '$cordovaImagePicker', '$cordovaCamera', '$cordovaToast', '$ionicPopup', function ($scope, $http, $config, $customlocalstorage, $ionicLoading, $stateParams, $cordovaImagePicker, $cordovaCamera, $cordovaToast, $ionicPopup) {
        $scope.product = $stateParams.product;
        $scope.image = {
            primaryUrl: 'http://' + $config.IP_PORT + '/product/image/' + $scope.product.id + '/1',
            dUrl: [[]]
        }
        var rowImageCount = 2;
        for (var count = 2; count <= 7; count++) {
            if ($scope.image.dUrl[$scope.image.dUrl.length - 1].length == 2) {
                $scope.image.dUrl.push([]);
            }
            $scope.image.dUrl[$scope.image.dUrl.length - 1].push({ url: 'http://' + $config.IP_PORT + '/product/image/' + $scope.product.id + '/' + count + '?' + new Date(), index: count });
        }
        $scope.imageData = '';
        var selectionPopup;
        var options = {};
        $scope.uploadProductImage = function (imageIndex) {
            $scope.data = {};
            var uploadImage = function () {
                $cordovaCamera.getPicture(options).then(function (imageData) {
                    $scope.imageData = imageData;

                    var opt = new FileUploadOptions();
                    opt.fileKey = "image_file";
                    opt.fileName = imageData.substr(imageData.lastIndexOf('/') + 1);
                    opt.mimeType = "image/jpeg";
                    opt.params = {
                        product_id: $scope.product.id,
                        image_type: imageIndex
                    };

                    var ft = new FileTransfer();
                    ft.onprogress = function (progressEvent) {
                        $scope.image.progress = JSON.stringify(progressEvent);
                        console.log(progressEvent);
                    };

                    $ionicLoading.show({
                        template: 'Uploading product image...',
                        duration: 20000
                    })
                    ft.upload(imageData, encodeURI("http://" + $config.IP_PORT + "/product/uploadImageFile"),
                    function (res) {
                        $ionicLoading.hide();
                        $cordovaToast.showShortBottom('Product image uploaded');
                        if (imageIndex == 1) {
                            $scope.image.primaryUrl = 'http://' + $config.IP_PORT + '/product/image/' + $scope.product.id + '/1?' + new Date()
                        }
                        else {
                            $scope.image.dUrl[Math.floor((imageIndex - 2) / 2)][(imageIndex - 2) % 2].url = 'http://' + $config.IP_PORT + '/product/image/' + $scope.product.id + '/' + imageIndex + '?' + new Date();
                        }
                    },
                    function (err) {
                        $ionicLoading.hide();
                        $cordovaToast.showShortBottom('Error uploading the Product image. Please try again later.');
                    }, opt);

                }, function (err) {
                    console.log(err);
                    $cordovaToast.show('Error getting image from device.', 'bottom', false, 3000);
                });
            };
            $scope.getFromCamera = function () {
                selectionPopup.close();
                options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.NATIVE_URI,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 200,
                    targetHeight: 200,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true,
                    correctOrientation: true
                };
                uploadImage();
            };
            $scope.getFromFile = function () {
                selectionPopup.close();
                options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                    allowEdit: true
                };
                uploadImage();
            };

            selectionPopup = $ionicPopup.show({
                template: '<div class="list card"><a class="item item-icon-left" ng-click="getFromCamera()"><i class="icon ion-camera"></i>Camera</a><a class="item item-icon-left" ng-click="getFromFile()"><i class="icon ion-image"></i>Existing file</a></div>',
                title: 'Take image from?',
                scope: $scope
            });
        };
    }])
})();










