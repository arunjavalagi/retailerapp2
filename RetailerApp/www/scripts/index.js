﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        var Pushbots = PushbotsPlugin.initialize("56a64a3317795906438b456a", { "android": { "sender_id": "942601407343" } });

        Pushbots.on("registered", function (token) {
            localStorage['token'] = token;
            navigator.notification.alert("token:" + token);
            console.log(token);
        });

        Pushbots.getRegistrationId(function (token) {
            console.log("Registration Id:" + token);
            navigator.notification.alert("token:" + token);
        });
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
    };
    document.addEventListener("offline", function () {
        $('#conn-err-img').show();
        navigator.notification.alert(
        'You went offline! Please connect to internet to continue.',
        function () { },
        'Network Status',
        'OK'
        );
    }, false);
    document.addEventListener("online", function () {
        $('#conn-err-img').hide();
    }, false);

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
})();

function parseDate(date) {
    return new Date(
        date.substring(6, 10),
        date.substring(3, 5),
        date.substring(0, 2),
        date.substring(11, 13),
        date.substring(14, 16),
        date.substring(17, 19)
        );
}
function minifyDate(date) {
    var todayBegin = new Date();
    todayBegin.setHours(0);
    todayBegin.setMinutes(0);
    todayBegin.setSeconds(0);
    if (todayBegin < date) {
        return date.toTimeString().substring(0, 5);
    }
    var yesterdayBegin = todayBegin.setHours(-24);
    if (yesterdayBegin < date) {
        return 'YESTERDAY';
    }
    return date.toDateString().substring(4);
}