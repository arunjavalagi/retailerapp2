﻿(function () {
    "use strict";

    angular.module("myapp", ["ionic", "myapp.controllers", "myapp.services", 'ngMessages'])
        .run(function ($ionicPlatform, $rootScope) {
            $ionicPlatform.ready(function () {
                if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
            });
            //$rootScope.SERVER = "eepls-api.cfapps.io";
            $rootScope.SERVER = "nabl.tk:8080";
            $rootScope.newOrderCount = 0;
            $rootScope.orderCompletedStatus = 0;
        })
        .config(function ($stateProvider, $urlRouterProvider) {
            $stateProvider
            .state("app", {
                url: "/app",
                abstract: true,
                templateUrl: "app/templates/view-menu.html",
                controller: "appCtrl"
            })
            .state("home", {
                url: "/home",
                cache: false,
                templateUrl: "app/templates/view-home.html",
                controller: "homeCtrl"
            })
            .state("orderDetail", {
                url: "/orderDetail",
                templateUrl: "app/templates/view-order-detail.html",
                controller: "orderDetailCtrl",
                params: {
                    order: '',
                    allStatus: []
                }
            })
            .state("printPreview", {
                url: "/printPreview",
                templateUrl: "app/templates/view-printpreview.html",
                controller: "printCtrl",
                params: {
                    orderDetail: ''
                }
            })
            .state("login", {
                url: "/login",
                templateUrl: "app/templates/view-login.html",
                controller: "loginCtrl"
            })
            .state("invitation", {
                url: "/invitation",
                templateUrl: "app/templates/view-invite-customer.html",
                controller: "invitationCtrl"
            })
            .state("register", {
                url: "/register",

                templateUrl: "app/templates/view-register.html",
                controller: "registerCtrl"
            })
            .state("profile", {
                url: "/profile",
                templateUrl: "app/templates/view-retailerprofile.html",
                controller: 'retailerProfileCtrl'
            })
           .state("editprofile", {
               url: "/editprofile",
               templateUrl: "app/templates/view-editprofile.html",
               controller: "editprofileCtrl"
           })
            .state("addproducts", {
                url: "/addproducts",
                templateUrl: "app/templates/view-addproducts.html",
                controller: "addproductsCtrl",
                params: { product: '' }
            })
            .state("newrrders", {
                url: "/neworders",
                templateUrl: "app/templates/view-neworders.html",
                controller: "newordersCtrl"
            })
            .state("settings", {
                url: "/settings",
                templateUrl: "app/templates/view-settings.html",
                controller: "settingsCtrl"
            })
            .state('productmanagement', {
                url: '/productmanagement',
                templateUrl: 'app/templates/view-productmanagement.html',
                controller: 'productManagementCtrl'
            })
            .state('addedProducts', {
                url: '/addedProducts',
                templateUrl: 'app/templates/view-addedproducts.html',
                controller: 'addedProductsCtrl'
            })
            .state('productDetail', {
                url: '/productDetail',
                templateUrl: 'app/templates/view-productdetail.html',
                params: {
                    product: ''
                },
                controller: 'productDetailCtrl'
            })
            .state('addProductImage', {
                url: '/addProductImage',
                templateUrl: 'app/templates/view-addproductimage.html',
                controller: 'addProductImageCtrl',
                params: {
                    product: ''
                }
            })
            .state("terms", {
                url: '/terms',
                templateUrl: 'app/templates/view-terms.html'
            })
            ;
            if (localStorage['loggedIn'] === 'true') {
                console.log('logged In');
                $urlRouterProvider.otherwise("/home");
            }
            else {
                $urlRouterProvider.otherwise("/login");
            }

            //  $urlRouterProvider.otherwise("/register");
        });
})();